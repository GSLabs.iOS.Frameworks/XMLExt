//
//  XMLExt.h
//  XMLExt
//
//  Created by Dan Kalinin on 11/25/20.
//

#import <XMLExt/XmleMain.h>
#import <XMLExt/XmleInit.h>

FOUNDATION_EXPORT double XMLExtVersionNumber;
FOUNDATION_EXPORT const unsigned char XMLExtVersionString[];
